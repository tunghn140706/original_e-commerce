<%-- 
    Document   : checkout
    Created on : Nov 10, 2020, 8:50:35 AM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Checkout Page</title>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="style.css">
    </head>
    <body>
        <div class="container">
            <div class="navbar">
                <div class="logo">
                    <img src="images/logo.png" width="125px">
                </div>
                <nav>
                    <ul id="MenuItems"> 
                        <li><a href="Home">Home</a></li>
                        <li><a href="allProduct">Product</a></li>
                        <li><a href="">About</a></li>
                        <li><a href="">Contact</a></li>
                        <li><a href="logout">Log Out</a></li>
                    </ul>
                </nav>
                <a href="CartServlet"><img src="images/cart.png" width="30px" height="30px"></a>
                <img src="images/menu.png" class="menu-icon" onclick="menutoggle()">

            </div>
        </div>

        <div class="form-row container-fluid">
            <div class="mt-3 col-sm-8">
                <h4 class="mb-2">Fill all those field in order to complete your order!</h4>
                <form action="checkout" method="post" >
                    <div class="form-row">
                        <div class="form-group col-sm-6">
                            <label for="myEmail">First Name</label>
                            <input type="text" class="form-control"
                                   id="myEmail" placeholder="First Name">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="myPassword">Last Name</label>
                            <input type="text" class="form-control"
                                   id="myPassword" placeholder="Last Name">
                        </div>
                    </div>
                    <div class="form-group col-sm-12" style="padding: 0">
                        <label for="inputAddress">Address</label>
                        <input type="text" class="form-control"
                               id="myAddress" placeholder="1234 Main St">
                    </div>
                    <div class="form-group col-sm-12" style="padding: 0">
                        <label for="inputAddress2">Address 2</label>
                        <input type="text" class="form-control"
                               id="myAddress2" placeholder="Apartment, studio, or floor">
                    </div>
                    <div class="form-row">
                        <div class="form-group col-sm-5">
                            <label for="myCity">City</label>
                            <input type="text" class="form-control" id="myCity"
                                   placeholder="Eg: New York, etc">
                        </div>
                        <div class="form-group col-sm-4">
                            <label for="myState">State</label>
                            <input  id="myState" class="form-control" type="text">
                        </div>
                        <div class="form-group col-sm-3">
                            <label for="myPhone">Phone Number</label>
                            <input type="number" class="form-control" id="myPhone">
                        </div>
                    </div>
                    <div class="form-group col-sm-12" style="padding: 0">
                        <label for="Note">Other notes for shipping process</label>
                        <input type="text" class="form-control"
                               id="myAddress2" placeholder="Eg: What time will you want to receive the package?">
                    </div>


                    <div class="form-group">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" id="myCheck">
                            <label class="form-check-label" for="myCheck">
                                Agree with the payment
                            </label>
                        </div>
                    </div>
                    <input type="submit" class="btn btn-primary" value = "Check Out">
                </form>
            </div>
            <div class = "total-price col-sm-4 mt-5 pt-5">
                <%
                            if(request.getParameter("totalPrice") != null){
                        %>
                <table>
                    <tr>
                        <td>Subtotal</td>
                        
                        <td>$<%=request.getParameter("totalPrice") %></td>
                    </tr>

                    <tr>
                        <td>Tax</td>
                        <td>$<%=Double.parseDouble(request.getParameter("totalPrice"))/10 %></td>
                    </tr>
                    <!--                    .divide(BigDecimal.TEN)-->
                    <tr>
                        <td>Total</td>
                        <td>$<%=Double.parseDouble(request.getParameter("totalPrice"))*1.1 %></td>
                    </tr>
                </table>
                    
                   <%
                   }
                   %>
            </div>
        </div>        

        <div class="footer">
            <div class="container">
                <div class="row">
                    <div class="footer-col-1">
                        <h3>Download Our App</h3>
                        <p>Download App For Android and IOS Mobile Phone.</p>
                        <div class="app-logo">
                            <img src="images/play-store.png">
                            <img src="images/app-store.png">
                        </div>
                    </div>
                    <div class="footer-col-2">
                        <img src="images/logo-white.png" >
                        <p>Our Purpose is Subtainably Make The Pleasure and Benefits Of Sports Accessible to the Many.</p>
                    </div>
                    <div class="footer-col-3">
                        <h3>Useful Links</h3>
                        <ul>
                            <li>Coupons</li>
                            <li>Blog Post</li>
                            <li>Return Policy</li>
                            <li>Join Affiliate</li>
                        </ul>
                    </div>
                    <div class="footer-col-4">
                        <h3>Follow Us</h3>
                        <ul>
                            <li>Facebook</li>
                            <li>Twitter</li>
                            <li>Instagram</li>
                            <li>Youtube</li>
                        </ul>
                    </div>
                </div>
                <hr>
            </div>
        </div>
        <script>
            var MenuItems = document.getElementById("MenuItems");
            MenuItems.style.maxHeight = "0px";
            function menutoggle() {
                if (MenuItems.style.maxHeight == "0px") {
                    MenuItems.style.maxHeight = "200px";
                } else {
                    MenuItems.style.maxHeight = "0px";
                }
            }
        </script>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    </body>
</html>
