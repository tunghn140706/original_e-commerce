/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.LineProduct;
import Model.Order;
import Model.ShoppingCart;
import connect.DBContext2;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Admin
 */
public class OrderDAO extends DBContext2 {

    public void saveOrder(Order order) {
        int orderId = 0;
        Date currentDate = Calendar.getInstance().getTime();
        ShoppingCart cart = order.getCart();
        List<LineProduct> list = new ArrayList<>();
        list = cart.listItemsInCart();
        BigDecimal totalPrice = BigDecimal.ZERO;
        for (LineProduct l : list) {
            totalPrice = totalPrice.add(l.getPrice().multiply(BigDecimal.valueOf(l.getQuantity())));
        }

        // insert into table order
        try {
            String sql = "insert into [Order] ([UserId], [Name], [PhoneNumber], [Date], [ShippingAddress], [City], [State], [TotalPayment], [Note])\n"
                    + "values ('?',?, ?, getDate(), ?, ?, ?, ?, ?)";
            PreparedStatement statement = con.prepareStatement(sql);
            statement.setInt(1, order.getUserId());
            statement.setString(2, order.getCustomerName());
            statement.setString(3, order.getPhoneNumber());
            statement.setString(4, String.valueOf(currentDate));
            statement.setString(5, order.getShippingAddress());
            statement.setString(6, order.getCity());
            statement.setString(7, order.getState());
            statement.setBigDecimal(8, totalPrice);
            statement.setString(9, order.getNote());

            statement.executeUpdate(sql);
        } catch (Exception e) {
            System.out.println("Inserting Order Error! " + e.getStackTrace());
        }

        
        // get order id from recent order
        try {
            String sql = "select id\n"
                    + "from [Order]\n"
                    + "where UserID = ?\n"
                    + "and Name = ?\n"
                    + "and PhoneNumber = 11999999\n"
                    + "and [Date] = '?'\n"
                    + "and TotalPayment = ?\n"
                    + "and Note = ?";
            PreparedStatement statement = con.prepareStatement(sql);
            statement.setInt(1, order.getUserId());
            statement.setString(2, order.getCustomerName());
            statement.setString(3, order.getPhoneNumber());
            statement.setString(4, String.valueOf(currentDate));
            statement.setString(5, order.getState());
            statement.setBigDecimal(6, totalPrice);
            statement.setString(7, order.getNote());
            ResultSet rs = statement.executeQuery();
            if(rs.next()){
                 orderId = rs.getInt(1);
            }    
        } catch (Exception e) {
            System.out.println("Getting orderId error! " + e.getStackTrace());
        }

        //insert into order details
        for (LineProduct l : list) {
            try {
                String productID = l.getProductId();
                int quantity = l.getQuantity();
                String sql = "insert into [OrderDetail]"
                        + "values(?, '?', ?, ?)";                            // con can sua lai cau lenh sql
                PreparedStatement statement = con.prepareStatement(sql);
                statement.setInt(1, orderId);
                statement.setString(2, l.getProductId());
                statement.setInt(3, quantity);
                statement.setBigDecimal(4, l.getPrice().multiply(new BigDecimal(quantity)));

                statement.executeUpdate(sql);

            } catch (Exception e) {
                System.out.println("Inserting into order detail Error: " + e.getStackTrace());
            }

        }

    }
}
