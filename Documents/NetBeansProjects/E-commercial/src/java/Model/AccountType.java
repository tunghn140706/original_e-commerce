/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Admin
 */
public enum AccountType {
    ADMIN("admin"),
    CUSTOMER("customer");
    private final String name;
    private AccountType(String s){
        name = s;
    }
    @Override
    public String toString(){
        return this.name;
    }
}
