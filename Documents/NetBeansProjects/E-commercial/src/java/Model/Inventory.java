/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Admin
 */
public class Inventory {
    private Map<String, Product> products = new HashMap<>();
    
    public void add(Product item){
        this.products.put(item.getId(), item);
    }
    
    public Product get(String id){
        return this.products.get(id);
    }
    
}
