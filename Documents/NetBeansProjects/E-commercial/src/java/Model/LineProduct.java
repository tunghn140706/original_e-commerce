/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.math.BigDecimal;

/**
 *
 * @author Admin
 */
public class LineProduct {
    private String productId;
    private int quantity;
    private BigDecimal price;
    private String name;

    public LineProduct(String item, int quantity) {
        this.productId = item;
        this.quantity = quantity;
    }
    
    public LineProduct(String item) {
        this(item, 1);
    }
    
    public String getProductId() {
        return productId;
    }

    public int getQuantity() {
        return quantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }
    

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 31 * hash + this.quantity;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final LineProduct other = (LineProduct) obj;
        if (this.quantity != other.quantity) {
            return false;
        }
        return true;
    }
    
    public void reduceByQuantity(int ruduceNumber){
        this.quantity -= ruduceNumber;
    }
    
    public  BigDecimal totalPrice(){
        return price.multiply(BigDecimal.valueOf(quantity));
    }

    void increaseQuantityBy(int quantity) {
        this.quantity += quantity;
    }
        
}
