/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.math.BigDecimal;

/**
 *
 * @author Admin
 */
public class Product {
    private final String id;
    private final String name;
    private final ProductType productType;
    private final BigDecimal price;
    private final String description;
    private final String img;
    
    
    // immuatble object: cannot be change after intializing
    // only get, not set
    
 
    public Product(String id, String name, ProductType productType, BigDecimal price, String Description, String img) {
        super();
        this.id = id;
        this.name = name;
        this.productType = productType;
        this.price = price;
        this.description = Description;
        this.img = img;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }
    
    public ProductType getProductType() {
        return productType;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public String getDescription() {
        return description;
    }

    public String getImg() {
        return img;
    }
    

    public static class ProductBuilder{
    
        private  String id;
        private  String name;
        private  ProductType productType;
        private  BigDecimal price;
        private String description;
        private String img;
        
        public ProductBuilder(String id, String name){
            this.id = id;
            this.name = name;
        }
        
        public ProductBuilder withProductType(ProductType productType){
            this.productType = productType;
            return this;
        }
        
        public ProductBuilder withPrice(BigDecimal price){
            this.price = price;
            return this;
        }
        public ProductBuilder withDescription(String Description){
            this.description = Description;
            return this;
        }
        public ProductBuilder withImg(String img){
            this.img = img;
            return this;
        }
        
        public Product build(){
            Product item = new Product(this.id, this.name, this.productType,this.price,this.description, this.img);
            return item;
        }
    }

}

    


