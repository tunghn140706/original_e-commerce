/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Admin
 */
public enum ProductType {
    CLOTHING("CLOTHING"), 
    SHOES("SHOES"), 
    ACCESSORIES("ACCESSORIES");
    private final String name;
    
    private ProductType(String s){
        name = s;
    }
    @Override
    public String toString(){
        return this.name;
    }
}
