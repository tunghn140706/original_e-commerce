/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Admin
 */
public class ShoppingCart {

    private final Inventory inventory;
    private Map<String, LineProduct> productInCart = new LinkedHashMap<>();

    public ShoppingCart(Inventory inventory) {
        this.inventory = inventory;
    }

    public void addItem(LineProduct lineProduct) {
        // if choosen items are already in the cart, then just add to initial number
        if (productInCart.containsKey(lineProduct.getProductId())) {
            LineProduct lineProductInCart = productInCart.get(lineProduct.getProductId());
            lineProductInCart.increaseQuantityBy(lineProduct.getQuantity());
        }
        // if items are new, just add them to the list
        else {
            Product item = inventory.get(lineProduct.getProductId());                      //quan trong de lay san pham tu lineitem
            lineProduct.setName(item.getName());
            lineProduct.setPrice(item.getPrice());
            this.productInCart.put(lineProduct.getProductId(), lineProduct);
        }
    }
    
    // caculate total number of items in cart
    public int totalNumberOfItems() {
        int totalItems = 0;
        for (LineProduct lineItem : productInCart.values()) {
            totalItems = totalItems + lineItem.getQuantity();
        }
        return totalItems;
    }

    // remove items from cart
    public void remove(LineProduct removedLineItem) {
        for (LineProduct itemInCart : productInCart.values()) {
            //find in the list of LineProduct to find the one which is equal with the one user want to remove
            if (itemInCart.getProductId().equals(removedLineItem.getProductId())) {
                // if the number of removed items is equal or biger than the in the cart
                // remove all that items in cart
                if (removedLineItem.getQuantity() >= itemInCart.getQuantity()) {
                    this.productInCart.remove(removedLineItem.getProductId());
                    break;
                } 
                // on the contrary, just remove the number of cart that user want
                else {
                    itemInCart.reduceByQuantity(removedLineItem.getQuantity());
                }
            }
        }
    }

    // return list of LineProduct in cart
    public List<LineProduct> listItemsInCart() {
        return Collections.unmodifiableList(new ArrayList<>(this.productInCart.values()));
    }

}
