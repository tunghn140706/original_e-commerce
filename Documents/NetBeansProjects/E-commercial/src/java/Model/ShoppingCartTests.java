/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Before;
import org.junit.Test;


/**
 *
 * @author Admin
 */
public class ShoppingCartTests {

    private Inventory inventory;
    private ShoppingCart cart;
    Product item1 = new Product.ProductBuilder("Item1", "Giay").withProductType(ProductType.CLOTHING).withPrice(BigDecimal.valueOf(4000)).build();
    Product item2 = new Product.ProductBuilder("Item-2", "Ao").withProductType(ProductType.ACCESSORIES).withPrice(BigDecimal.valueOf(4000)).build();
    Product item3 = new Product.ProductBuilder("Item-3", "Quan").withProductType(ProductType.SHOES).withPrice(BigDecimal.valueOf(4000)).build();

    @Before
    public void setUp() throws Exception {                  // set up inventory // important 

        inventory = new Inventory();
        inventory.add(item1);
        inventory.add(item3);
        inventory.add(item2);   
        cart = new ShoppingCart(inventory);
    }

    @Test
    public void should_add_an_new_item_to_a_cart() {
        cart.addItem(new LineProduct(item1.getId()));

        int totalItemCount = cart.totalNumberOfItems();
        assertThat(totalItemCount).isEqualTo(1);
    }

    @Test
    public void should_add_multiple_items_to_the_cart() {
        cart.addItem(new LineProduct(item1.getId()));
        cart.addItem(new LineProduct(item2.getId()));
        cart.addItem(new LineProduct(item3.getId()));

        int totalItemCount = cart.totalNumberOfItems();
        assertThat(totalItemCount).isEqualTo(3);
    }

    @Test
    public void should_add_multiple_quantity_of_the_same_item_to_the_cart() {
        cart.addItem(new LineProduct(item2.getId()));

        int totalItemCount = cart.totalNumberOfItems();
        assertThat(totalItemCount).isEqualTo(3);
    }
    
    @Test
    public void should_remove_item_from_the_cart(){
        cart.addItem(new LineProduct(item2.getId()));
        cart.addItem(new LineProduct(item1.getId()));
        
        int totalItemCount = cart.totalNumberOfItems();
        cart.remove(new LineProduct(item2.getId()));
        assertThat(totalItemCount).isEqualTo(1);
    }
    
    @Test
    public void should_remove_specific_quantity_item_from_the_cart(){
        cart.addItem(new LineProduct(item2.getId(), 3));
        cart.addItem(new LineProduct(item1.getId(), 4));
        
        int totalItemCount = cart.totalNumberOfItems();
        cart.remove(new LineProduct(item2.getId(), 2));
        assertThat(totalItemCount).isEqualTo(5 );
    }
    
    @Test
    public void should_view_listing_of_items_in_the_cart(){
        cart.addItem(new LineProduct(item2.getId(), 3));
        cart.addItem(new LineProduct(item1.getId(), 4));
        
        List<LineProduct> lineItems = cart.listItemsInCart();
        assertThat(lineItems.get(0).totalPrice()).isEqualTo(BigDecimal.valueOf(12000));
        assertThat(lineItems.get(1).totalPrice()).isEqualTo(BigDecimal.valueOf(16000));

    }
    
    @Test
    public void should_increse_the_quantity_of_items_when_adding_the_same_for_multiple_times(){
        cart.addItem(new LineProduct(item1.getId(), 3));
        cart.addItem(new LineProduct(item1.getId(), 4));
        
        int totalItemCount = cart.totalNumberOfItems();
        
        assertThat(totalItemCount).isEqualTo(7);
        
        cart.remove(new LineProduct(item1.getId(), 6));
        
        totalItemCount = cart.totalNumberOfItems();
        
        assertThat(totalItemCount).isEqualTo(1);
    }
}
