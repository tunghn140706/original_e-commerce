USE [master]
GO
/****** Object:  Database [Market]    Script Date: 2/22/2021 5:02:24 PM ******/
CREATE DATABASE [Market]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Market', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\Market.mdf' , SIZE = 3264KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Market_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\Market_log.ldf' , SIZE = 816KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Market] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Market].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Market] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Market] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Market] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Market] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Market] SET ARITHABORT OFF 
GO
ALTER DATABASE [Market] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [Market] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Market] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Market] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Market] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Market] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Market] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Market] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Market] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Market] SET  ENABLE_BROKER 
GO
ALTER DATABASE [Market] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Market] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Market] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Market] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Market] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Market] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Market] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Market] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Market] SET  MULTI_USER 
GO
ALTER DATABASE [Market] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Market] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Market] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Market] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [Market] SET DELAYED_DURABILITY = DISABLED 
GO
USE [Market]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 2/22/2021 5:02:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Account](
	[ID] [int] NOT NULL,
	[Username] [varchar](50) NOT NULL,
	[Password] [varchar](50) NOT NULL,
	[Role] [varchar](50) NOT NULL,
	[Description] [varchar](300) NULL,
 CONSTRAINT [PK_Account] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 2/22/2021 5:02:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nchar](150) NULL,
	[Company] [nchar](150) NULL,
	[Address1] [nchar](250) NULL,
	[Address2] [nchar](250) NULL,
	[Zip] [nchar](150) NULL,
	[City] [nchar](250) NULL,
	[State] [nchar](150) NULL,
	[Phone] [int] NULL,
	[Country] [nchar](100) NULL,
	[Email] [nchar](100) NULL,
	[Comment] [nchar](250) NULL,
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Order]    Script Date: 2/22/2021 5:02:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Order](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [nvarchar](50) NULL,
	[Name] [nvarchar](100) NULL,
	[PhoneNumber] [nvarchar](10) NULL,
	[Date] [nvarchar](50) NULL,
	[ShippingAddress] [nvarchar](300) NULL,
	[City] [nvarchar](100) NULL,
	[State] [nvarchar](100) NULL,
	[TotalPayment] [float] NULL,
	[Note] [nvarchar](500) NULL,
 CONSTRAINT [PK_Order] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[OrderDetail]    Script Date: 2/22/2021 5:02:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderDetail](
	[OrderID] [int] NULL,
	[ProductID] [nvarchar](50) NULL,
	[Quantity] [int] NULL,
	[Price] [float] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Product]    Script Date: 2/22/2021 5:02:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Product](
	[id] [int] NOT NULL,
	[Name] [varchar](150) NULL,
	[Price] [varchar](10) NULL,
	[Description] [varchar](1000) NULL,
	[Image] [varchar](150) NULL,
	[Type] [varchar](50) NULL,
 CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Account] ([ID], [Username], [Password], [Role], [Description]) VALUES (1, N'admin', N'1234', N'ADMIN', N'Control system                                                                                                                                                                                                                                                                                              ')
INSERT [dbo].[Account] ([ID], [Username], [Password], [Role], [Description]) VALUES (2, N'tung', N'123', N'CUSTOMER', N'Naughty                                                                                                                                                                                                                                                                                                     ')
INSERT [dbo].[Account] ([ID], [Username], [Password], [Role], [Description]) VALUES (3, N'minh', N'321', N'CUSTOMER', N'Adorable                                                                                                                                                                                                                                                                                                    ')
SET IDENTITY_INSERT [dbo].[Customer] ON 

INSERT [dbo].[Customer] ([id], [Name], [Company], [Address1], [Address2], [Zip], [City], [State], [Phone], [Country], [Email], [Comment]) VALUES (23, N'd1                                                                                                                                                    ', N'1999                                                                                                                                                  ', N'19                                                                                                                                                                                                                                                        ', N'ss                                                                                                                                                                                                                                                        ', N'ss                                                                                                                                                    ', N'11                                                                                                                                                                                                                                                        ', N'2                                                                                                                                                     ', 919104743, N'11                                                                                                  ', N'gicungduoc1999@gmail.com                                                                            ', N'd1                                                                                                                                                                                                                                                        ')
INSERT [dbo].[Customer] ([id], [Name], [Company], [Address1], [Address2], [Zip], [City], [State], [Phone], [Country], [Email], [Comment]) VALUES (24, N'd1                                                                                                                                                    ', N'1999                                                                                                                                                  ', N'19                                                                                                                                                                                                                                                        ', N'ss                                                                                                                                                                                                                                                        ', N'ss                                                                                                                                                    ', N'11                                                                                                                                                                                                                                                        ', N'2                                                                                                                                                     ', 919104743, N'11                                                                                                  ', N'gicungduoc1999@gmail.com                                                                            ', N'd1                                                                                                                                                                                                                                                        ')
INSERT [dbo].[Customer] ([id], [Name], [Company], [Address1], [Address2], [Zip], [City], [State], [Phone], [Country], [Email], [Comment]) VALUES (25, N'd2                                                                                                                                                    ', N'1999                                                                                                                                                  ', N'19                                                                                                                                                                                                                                                        ', N'ss                                                                                                                                                                                                                                                        ', N'ss                                                                                                                                                    ', N'11                                                                                                                                                                                                                                                        ', N'2                                                                                                                                                     ', 919104743, N'11                                                                                                  ', N'gicungduoc1999@gmail.com                                                                            ', N'd2                                                                                                                                                                                                                                                        ')
INSERT [dbo].[Customer] ([id], [Name], [Company], [Address1], [Address2], [Zip], [City], [State], [Phone], [Country], [Email], [Comment]) VALUES (26, N'1                                                                                                                                                     ', N'1999                                                                                                                                                  ', N'19                                                                                                                                                                                                                                                        ', N'ss                                                                                                                                                                                                                                                        ', N'ss                                                                                                                                                    ', N'11                                                                                                                                                                                                                                                        ', N'2                                                                                                                                                     ', 919104743, N'11                                                                                                  ', N'gicungduoc1999@gmail.com                                                                            ', N'1                                                                                                                                                                                                                                                         ')
INSERT [dbo].[Customer] ([id], [Name], [Company], [Address1], [Address2], [Zip], [City], [State], [Phone], [Country], [Email], [Comment]) VALUES (27, N'2                                                                                                                                                     ', N'1999                                                                                                                                                  ', N'19                                                                                                                                                                                                                                                        ', N'ss                                                                                                                                                                                                                                                        ', N'ss                                                                                                                                                    ', N'11                                                                                                                                                                                                                                                        ', N'2                                                                                                                                                     ', 919104743, N'11                                                                                                  ', N'gicungduoc1999@gmail.com                                                                            ', N'2                                                                                                                                                                                                                                                         ')
INSERT [dbo].[Customer] ([id], [Name], [Company], [Address1], [Address2], [Zip], [City], [State], [Phone], [Country], [Email], [Comment]) VALUES (28, N'3                                                                                                                                                     ', N'1999                                                                                                                                                  ', N'19                                                                                                                                                                                                                                                        ', N'ss                                                                                                                                                                                                                                                        ', N'ss                                                                                                                                                    ', N'11                                                                                                                                                                                                                                                        ', N'2                                                                                                                                                     ', 919104743, N'11                                                                                                  ', N'gicungduoc1999@gmail.com                                                                            ', N'3                                                                                                                                                                                                                                                         ')
INSERT [dbo].[Customer] ([id], [Name], [Company], [Address1], [Address2], [Zip], [City], [State], [Phone], [Country], [Email], [Comment]) VALUES (29, N'a1                                                                                                                                                    ', N'1999                                                                                                                                                  ', N'19                                                                                                                                                                                                                                                        ', N'ss                                                                                                                                                                                                                                                        ', N'ss                                                                                                                                                    ', N'11                                                                                                                                                                                                                                                        ', N'2                                                                                                                                                     ', 919104743, N'11                                                                                                  ', N'gicungduoc1999@gmail.com                                                                            ', N'a1                                                                                                                                                                                                                                                        ')
INSERT [dbo].[Customer] ([id], [Name], [Company], [Address1], [Address2], [Zip], [City], [State], [Phone], [Country], [Email], [Comment]) VALUES (30, N'a1                                                                                                                                                    ', N'1999                                                                                                                                                  ', N'19                                                                                                                                                                                                                                                        ', N'ss                                                                                                                                                                                                                                                        ', N'ss                                                                                                                                                    ', N'11                                                                                                                                                                                                                                                        ', N'2                                                                                                                                                     ', 919104743, N'11                                                                                                  ', N'gicungduoc1999@gmail.com                                                                            ', N'a1                                                                                                                                                                                                                                                        ')
SET IDENTITY_INSERT [dbo].[Customer] OFF
SET IDENTITY_INSERT [dbo].[Order] ON 

INSERT [dbo].[Order] ([ID], [UserID], [Name], [PhoneNumber], [Date], [ShippingAddress], [City], [State], [TotalPayment], [Note]) VALUES (8, N'1', N'Tung', N'11999999', N'Nov 10 2020  3:29PM', N'Tan Xa', N'Ha Noi', N'Thach That', 100000, N'Giao vao buoi sang')
INSERT [dbo].[Order] ([ID], [UserID], [Name], [PhoneNumber], [Date], [ShippingAddress], [City], [State], [TotalPayment], [Note]) VALUES (9, N'1', N'Tung', N'11999999', N'Nov 10 2020  3:31PM', N'Tan Xa', N'Ha Noi', N'Thach That', 100000, N'Giao vao buoi sang')
INSERT [dbo].[Order] ([ID], [UserID], [Name], [PhoneNumber], [Date], [ShippingAddress], [City], [State], [TotalPayment], [Note]) VALUES (10, N'1', N'Tung', N'11999999', N'Nov 10 2020  3:31PM', N'Tan Xa', N'Ha Noi', N'Thach That', 100000, N'Giao vao buoi sang')
INSERT [dbo].[Order] ([ID], [UserID], [Name], [PhoneNumber], [Date], [ShippingAddress], [City], [State], [TotalPayment], [Note]) VALUES (11, N'1', N'Tung', N'11999999', N'Nov 10 2020  3:33PM', N'Tan Xa', N'Ha Noi', N'Thach That', 100000, N'Giao vao buoi sang')
INSERT [dbo].[Order] ([ID], [UserID], [Name], [PhoneNumber], [Date], [ShippingAddress], [City], [State], [TotalPayment], [Note]) VALUES (12, N'1', N'Tung', N'11999999', N'Nov 10 2020  3:33PM', N'Tan Xa', N'Ha Noi', N'Thach That', 100000, N'Giao vao buoi sang')
INSERT [dbo].[Order] ([ID], [UserID], [Name], [PhoneNumber], [Date], [ShippingAddress], [City], [State], [TotalPayment], [Note]) VALUES (13, N'1', N'Tung', N'11999999', N'Nov 11 2020  3:35PM', N'Tan Xa', N'Ha Noi', N'Thach That', 100000, N'Giao vao buoi sang')
SET IDENTITY_INSERT [dbo].[Order] OFF
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [Quantity], [Price]) VALUES (28, N'0', 1, 2000)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [Quantity], [Price]) VALUES (28, N'0', 1, 2000)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [Quantity], [Price]) VALUES (28, N'0', 1, 300)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [Quantity], [Price]) VALUES (29, N'1', 1, 2000)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [Quantity], [Price]) VALUES (29, N'3', 1, 300)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [Quantity], [Price]) VALUES (29, N'2', 1, 2000)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [Quantity], [Price]) VALUES (30, N'1', 4, 2000)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [Quantity], [Price]) VALUES (30, N'3', 1, 300)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [Quantity], [Price]) VALUES (30, N'2', 1, 2000)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [Quantity], [Price]) VALUES (30, N'8', 1, 2000)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [Quantity], [Price]) VALUES (30, N'9', 1, 2000)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [Quantity], [Price]) VALUES (11, N'3', 2, 2000)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [Quantity], [Price]) VALUES (11, N'3', 2, 2000)
INSERT [dbo].[OrderDetail] ([OrderID], [ProductID], [Quantity], [Price]) VALUES (11, N'3', 2, 2000)
INSERT [dbo].[Product] ([id], [Name], [Price], [Description], [Image], [Type]) VALUES (2, N'Casual Shoes', N'68', N'This is the most suitable cloth', N'images/product-2.jpg', N'SHOES')
INSERT [dbo].[Product] ([id], [Name], [Price], [Description], [Image], [Type]) VALUES (3, N'Baggy Pants                                                                                                                                           ', N'30', N'This is the most suitable cloth for you', N'images/product-3.jpg', N'CLOTHING')
INSERT [dbo].[Product] ([id], [Name], [Price], [Description], [Image], [Type]) VALUES (4, N'Navy Polo', N'60', N'This is the most suitable cloth for you', N'images/product-4.jpg', N'CLOTHING')
INSERT [dbo].[Product] ([id], [Name], [Price], [Description], [Image], [Type]) VALUES (5, N'Maze Runner                                                                                                                                           ', N'70', N'This is the most suitable cloth for you', N'images/product-5.jpg', N'SHOES')
INSERT [dbo].[Product] ([id], [Name], [Price], [Description], [Image], [Type]) VALUES (6, N'Dark Age                                                                                                                                              ', N'40', N'This is the most suitable cloth for you', N'images/product-6.jpg', N'CLOTHING')
INSERT [dbo].[Product] ([id], [Name], [Price], [Description], [Image], [Type]) VALUES (7, N'Socks                                                                                                                                                 ', N'12', N'This is the most suitable cloth for you', N'images/product-7.jpg', N'CLOTHING')
INSERT [dbo].[Product] ([id], [Name], [Price], [Description], [Image], [Type]) VALUES (8, N'Classic Watch                                                                                                                                         ', N'200', N'This is the most suitable cloth for you', N'images/product-8.jpg', N'ACCESSORIES')
INSERT [dbo].[Product] ([id], [Name], [Price], [Description], [Image], [Type]) VALUES (9, N'Training Tracksuit Bottom                                                                                                                             ', N'42', N'This is the most suitable cloth for you', N'images/product-12.jpg', N'CLOTHING')
INSERT [dbo].[Product] ([id], [Name], [Price], [Description], [Image], [Type]) VALUES (10, N'Young Spirit Watch                                                                                                                                    ', N'50', N'This is the most suitable cloth for you', N'images/product-9.jpg', N'ACCESSORIES')
INSERT [dbo].[Product] ([id], [Name], [Price], [Description], [Image], [Type]) VALUES (11, N'Ultra Foam                                                                                                                                            ', N'34', N'This is the most suitable cloth for you', N'images/product-10.jpg', N'SHOES')
INSERT [dbo].[Product] ([id], [Name], [Price], [Description], [Image], [Type]) VALUES (12, N'Casual Shoes                                                                                                                                          ', N'30', N'This is the most suitable cloth for you', N'images/product-11.jpg', N'SHOES')
INSERT [dbo].[Product] ([id], [Name], [Price], [Description], [Image], [Type]) VALUES (13, N'Own The Rnn                                                                                                                                           ', N'45', N'This is the most suitable cloth for you', N'images/category-1.jpg', N'SHOES')
INSERT [dbo].[Product] ([id], [Name], [Price], [Description], [Image], [Type]) VALUES (14, N'Super Star                                                                                                                                            ', N'70', N'This is the most suitable cloth for you', N'images/category-2.jpg', N'SHOES')
INSERT [dbo].[Product] ([id], [Name], [Price], [Description], [Image], [Type]) VALUES (15, N'ZNE Coat                                                                                                                                              ', N'70', N'This is the most suitable cloth for you', N'images/category-3.jpg', N'CLOTHING')
INSERT [dbo].[Product] ([id], [Name], [Price], [Description], [Image], [Type]) VALUES (16, N'Dark Age II', N'30', N'This is the most suitable cloth for you', N'images/gallery-4.jpg', N'SHOES')
INSERT [dbo].[Product] ([id], [Name], [Price], [Description], [Image], [Type]) VALUES (34, N'Dark Night 6548', N'4343', N'Suitable for atheletic', N'images/gallery-2.jpg', N'ACCESSORIES')
USE [master]
GO
ALTER DATABASE [Market] SET  READ_WRITE 
GO
